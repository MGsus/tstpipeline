const express = require('express');
const router = express.Router();
const exec = require('child_process').exec;
const shell = require('shelljs');
const path = require('path');
const console = require('../../../log');

router.get('/', function (req, res) {

    exec('npm test', function (err, stdout, stderr) {
        console.log(stdout.split("✓").length - 1);

        res.format({
            'text/plain': () => { res.send(stdout) }
        });
    });
});

module.exports = router;