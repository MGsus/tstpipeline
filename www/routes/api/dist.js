var express = require('express');
var router = express.Router();
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const db = require('../../../config/db');
// const console = require('../../../log');

// Load input validation
const validateLoginInput = require('../../../validator/logIn');
const validateRegisterInput = require('../../../validator/signIn');

// GET api/dist with auth passport.authenticate('jwt', {session:false}), function(req,res)
router.get('/home', function (req, res) {
  // let params = req.params;
  // let body = req.body;
  // console.log('params:' + JSON.stringify(params));
  // console.log('body: ' + JSON.stringify(body));


  db.pool.query('SELECT cc, nom_dist, email, forma_pago, incoterm FROM distribuidores').then(rslt => {
    // res.status(200).xls('data.xlsx', rslt.rows);
    res.status(200).json(rslt.rows);
  }).catch(err => {
    console.error(err);
  });
});

// POST api/dist/login
router.post('/login', function (req, res) {
  const { err, isValid } = validateLoginInput(req.body);
  if (!isValid) return res.status(400).json(err);
  const { cc, password } = req.body;
  db.pool.query('SELECT * FROM distribuidores WHERE cc = $1', [cc])
    .then(rslt => {
      if (rslt.rows == 0) {
        res.status(200).json({
          state: false,
          message: "Usuario no encontrado"
        });
      } else {
        bcrypt.compare(password, rslt.rows[0].password)
          .then(isMatch => {
            if (isMatch) {
              const payload = {
                id: rslt.rows[0].id,
                nombre_dist: rslt.rows[0].nombre_dist,
                email: rslt.rows[0].email
              };

              jwt.sign(
                payload,
                "secret",
                {
                  expiresIn: '1h'
                }, function (err, token) {
                  console.log(token);
                  res.json({
                    success: true,
                    token: "Bearer " + token
                  });
                }
              );
            } else {
              return res
                .status(400)
                .json({ password: "Contraseña incorrecta" });
            }
          })
          .catch(err => { console.error(err); })
      }
    }).catch(err => { console.error(err); });
});

// POST api/dist/register
router.post('/register', function (req, res) {
  const { errors, isValid } = validateRegisterInput(req.body);
  if (!isValid) return res.status(400).json(errors);
  const { cc, nombre_dist, password, email, forma_pago, incoterm } = req.body;

  db.pool.query('SELECT * FROM distribuidores WHERE cc = $1', [cc])
    .then(user => {
      if (user.rows != 0) return res.status(200).json({ error: 'El usuario ya existe' });
      else {
        bcrypt.genSalt(10, function (err, salt) {
          if (err) console.error(err);
          bcrypt.hash(password, salt, function (err, hash) {
            if (err) console.error(err);
            db.pool.query('INSERT INTO distribuidores (cc, nombre_dist, password, email, forma_pago, incoterm) VALUES($1, $2, $3, $4, $5, $6)', [cc, nombre_dist, hash, email, forma_pago, incoterm])
              .then(res.status(200).json({ message: 'usuario creado!' }))
              .catch(err => console.error(err));
          })
        })
      }
    }).catch(err => { console.error(err); });
});

module.exports = router;
