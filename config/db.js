require('dotenv').config();
const { Pool } = require('pg');

const proPool = new Pool({
  user: 'production',
  host: 'https://vps-domain.com',
  database: 'proDB',
  port: 5432
});

const devPool = new Pool({
  user: 'gsus', // gsus
  host: '127.0.0.1',
  database: 'ctlgDB', // ctlgDB
  password: 'admin',
  port: 5432
});

const pool = process.env.NODE_ENV !== 'production' ? devPool : proPool;
module.exports = { pool };
